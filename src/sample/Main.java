package sample;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        //Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Log in");
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER); // allt som ligger i grid ligger i center
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25,25,25,25)); //Padding från kanterna
        Scene scene = new Scene(grid, 300,275);


        Text sceneTitle = new Text("Please log in.");
        Label userName = new Label("Enter Username");
        TextField userTextField = new TextField();
        Label pw = new Label("Enter Password");
        PasswordField pwBox = new PasswordField();

        grid.add(sceneTitle,0,0,2,1);
        grid.add(userName,0,1);
        grid.add(userTextField,1,1);
        grid.add(pw,0,2);
        grid.add(pwBox,1,2);

        scene.getStylesheets().add(Main.class.getResource("login.css").toExternalForm());

        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
